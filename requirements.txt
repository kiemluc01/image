Django==3.0.9
djangorestframework==3.14.0
django-cors-headers==3.5.0
django-heroku==0.3.1
gunicorn==20.0.4