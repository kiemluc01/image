from django.urls import path
from . import views

urlpatterns = [
    path('upload/', views.Upload_View.as_view())
]