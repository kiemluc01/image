from django.shortcuts import render
from rest_framework.response import Response
from django.core.files.storage import FileSystemStorage
from rest_framework import viewsets, status, views

from django.views.decorators.csrf import csrf_exempt
# Create your views here.
class Upload_View(views.APIView):
    def get(self, request):
        return Response(status.HTTP_200_OK)
    
    @csrf_exempt
    def post(self, request):
        links = []
        print(request.FILES)
        for image in request.FILES.getlist('image'):
            fs = FileSystemStorage()
            filename = fs.save(image.name, image)
            uploaded_file_url = 'https://image-qlbv.herokuapp.com/media/{}'.format(filename)
            links.append(uploaded_file_url)
        return Response({"links":links}, status=status.HTTP_200_OK)
